package util;

import java.io.Serializable;
import java.util.Date;

public class Address implements Serializable {

    private Long id;
    private PropertyDto type;
    private AddressLineDetail addressLineDetail;
    private PropertyDto provinceOrState;
    private PropertyDto country;
    private String suburbOrDistrict;
    private String postalCode;
    private String cityOrTown;
    private Date lastUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PropertyDto getType() {
        return type;
    }

    public void setType(PropertyDto type) {
        this.type = type;
    }

    public AddressLineDetail getAddressLineDetail() {
        return addressLineDetail;
    }

    public void setAddressLineDetail(AddressLineDetail addressLineDetail) {
        this.addressLineDetail = addressLineDetail;
    }

    public PropertyDto getProvinceOrState() {
        return provinceOrState;
    }

    public void setProvinceOrState(PropertyDto provinceOrState) {
        this.provinceOrState = provinceOrState;
    }

    public PropertyDto getCountry() {
        return country;
    }

    public void setCountry(PropertyDto country) {
        this.country = country;
    }

    public String getSuburbOrDistrict() {
        return suburbOrDistrict;
    }

    public void setSuburbOrDistrict(String suburbOrDistrict) {
        this.suburbOrDistrict = suburbOrDistrict;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCityOrTown() {
        return cityOrTown;
    }

    public void setCityOrTown(String cityOrTown) {
        this.cityOrTown = cityOrTown;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "Address {" +
                ", type=" + type.getName() +
                ", addressLineDetail=" + addressLineDetail.getLine1() +" ,"  + addressLineDetail.getLine2() +
                ", provinceOrState=" + provinceOrState.getName() +
                ", country=" + country.getName() + " " + country.getCode() +
                ", suburbOrDistrict='" + suburbOrDistrict + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", cityOrTown='" + cityOrTown + '\'' +
                '}';
    }
}
