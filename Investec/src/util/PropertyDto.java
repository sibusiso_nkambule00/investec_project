package util;

public class PropertyDto {

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PropertyDto() { }

    public PropertyDto(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
