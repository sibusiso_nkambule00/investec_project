
public class HighestCommonFactor {

	public static void main(String[] args) {
		int[] numbers = {8,12};
        System.out.print("HighestCommonFactor of N numbers is :");
        System.out.println(highestCommonFactor(numbers));
	}
	
	public static int highestCommonFactor(int[] numbers) {
        int highestCommonFactor=1;
        int index=2;
        
        if(numbers.length==1){
        	highestCommonFactor=numbers[1];
        }
        
        if(numbers.length>1){
        	highestCommonFactor= euclideanHighestCommonFactor(numbers[0],numbers[1]); 
        }
        
        while(index<numbers.length){
        	highestCommonFactor= euclideanHighestCommonFactor(highestCommonFactor,numbers[index]);
            index++;
        }
        return highestCommonFactor;
    }
	
	public static int euclideanHighestCommonFactor(int num1,int num2){
        int temp=0;
        while(num2!=0){
            temp=num2;
            num2=num1%num2;
            num1=temp;
        }
        num1=num1<0 ? num1 * (-1):num1;
        return num1;
    }

}
