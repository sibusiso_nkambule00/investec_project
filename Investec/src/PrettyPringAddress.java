import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import util.Address;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

public class PrettyPringAddress {

    private static final String filePath = "src/addresses.json";

    private  static List<Address> addresses = null;

    public static void main(String[] args) {


        Gson gson = new GsonBuilder().serializeNulls().create();

        try {

            Type classTpe = new TypeToken<List<Address>>() {}.getType();

            addresses = gson.fromJson(new FileReader(filePath), classTpe);

            System.out.println("\n\n =================== Address in the format: Type: Line details - city - province/state - postal code – country ===================\n\n ");
            for (Address address:  addresses) {
                System.out.println(prettyPrintAddress(address));
            }

            prettyPrintAllAddress();

            printAddressOfType("Physical");

            addressVerification();

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }



    }

    public static String  prettyPrintAddress(Address address) {

         return String.format("Type: %s - Line details: %s - city: %s - province/state: %s - postal code: %s – country: %s%n",
                 address.getType().getName(),
                 Objects.isNull(address.getAddressLineDetail()) ? null : address.getAddressLineDetail().getLine1() + ", " + address.getAddressLineDetail().getLine2(),
                 address.getCityOrTown(),
                 Objects.isNull(address.getProvinceOrState()) ? null : address.getProvinceOrState().getName(),
                 address.getPostalCode(),
                 address.getCountry().getName());

    }

    public static void  addressVerification(){
        System.out.println("\n\n ===================  Address Verification ===================\n\n");
        if (Objects.nonNull(addresses)) {
            for (Address address: addresses ) {
                String verifiedAddress = verifyAddress(address);
                System.out.println("Address verification  for " + address.getType().getName() + " : " + (Objects.isNull(verifiedAddress) ? "Address is valid" : verifiedAddress)) ;
            }
        }
    }

    public static String verifyAddress(Address address) {

        if (!isNumeric(address.getPostalCode())) {
            return  "Invalid address - Postal Code can only contain numeric character";
        }

        if (Objects.isNull(address.getAddressLineDetail())) {
            return "Invalid address - Address line detail is empty";
        }else if (Objects.nonNull(address.getAddressLineDetail()) && (
                Objects.isNull(address.getAddressLineDetail().getLine1()) ||
                        Objects.isNull(address.getAddressLineDetail().getLine2()))
        ){
            return "Invalid address - Address line 1 or 2 is empty";
        }

        if(Objects.isNull(address.getCountry())) {
            return "Invalid address: Country is required";
        }else if (Objects.nonNull(address.getCountry()) && address.getCountry().getCode().equals("ZA")) {
            if (Objects.isNull(address.getProvinceOrState())){
                return "Invalid address -Province is required";
            }
        }

        return  null;
    }

    public static boolean isNumeric(String value) {
        return Objects.nonNull(value) && value.matches("^[0-9]*$");

    }

    public static void prettyPrintAllAddress() {
        if (Objects.nonNull(addresses)) {
            System.out.println("\n\n =================== Pretty Print All the Address ===================");
            for (Address address: addresses) {
                System.out.printf("%n%n*** %s ***%n %s%n %s%n %s%n %s%n %s%n %s ",
                        address.getType().getName(),
                        Objects.isNull(address.getAddressLineDetail()) ? "" : address.getAddressLineDetail().getLine1() + ", " + address.getAddressLineDetail().getLine2(),
                        Objects.isNull(address.getSuburbOrDistrict()) ? "" : address.getSuburbOrDistrict(),
                        address.getCityOrTown(),
                        address.getPostalCode(),
                        Objects.isNull(address.getProvinceOrState()) ? "" : address.getProvinceOrState().getName(),
                        address.getCountry().getName());
            }
        }
    }

    public static void printAddressOfType(String addressType) {

        for (Address address: addresses ) {
            if (address.getType().getName().startsWith(addressType)){
                System.out.println("\n\n =================== Print Address By Type : " + addressType+  " ===================");
                System.out.println("\nAddress " + address.toString() );
                break;
            }
        }
    }
}